package yi.game.entities;

import yi.game.gfx.Screen;
import yi.game.level.Level;

public abstract class Entity {

	public int x, y;
	protected int id;
	protected Level level;
	protected int tickCount;

	public Entity(Level level, int x2, int y2) {
			
		this.level = level;
		this.x = x2;
		this.y = y2;
	}


	public void tick() {
		tickCount++;
	}
	
	public abstract void render(Screen screen);
	
}
