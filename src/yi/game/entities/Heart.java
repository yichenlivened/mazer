package yi.game.entities;

import yi.game.gfx.Colours;
import yi.game.gfx.Screen;
import yi.game.level.Level;

public class Heart extends Entity{


	private int animState;
	private boolean animDecr;
	
	public Heart(Level level, int x, int y) {
		super(level,x,y);
	}

	public void tick() {
		super.tick();

		if (tickCount % 6 == 0) {
			if (animDecr) {
				animState--;
			} else {
				animState++;
			}
			if (animState >= 3 || animState <= 0) {
				animDecr = !animDecr;
			}
		}
	}

	@Override
	public void render(Screen screen) {
		screen.render(x, y, 32 + animState, Colours.get(-1, -1, 300, 510), 0x00,
				1);
	}
	
}

